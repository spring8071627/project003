package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Gift;

public interface GiftRepository extends JpaRepository<Gift, Integer>{
	 @Query("select m from gift m where m.price >= :price")
	 List<Gift> get1(@Param("price") int price);
	
	 @Query("select m from gift name where m.name like %:name")
	 List<Gift> get2(@Param("name") String name);
	 
	 @Query("select m from gift m where m.price <= :price and type = :type")
	 List<Gift> get3(@Param("price") int price , @Param("type") String type);
	
}

//   JQPL
// @Query("select m from Memo m where m.no < :mno")
// List<Memo> get1(@Param("mno") int mno);
