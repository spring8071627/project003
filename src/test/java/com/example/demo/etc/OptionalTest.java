package com.example.demo.etc;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OptionalTest {

	@Test
	void OPtional사용하기(){
		Optional<String> opt = Optional.of("apple");
		System.err.println(opt.get());
		System.err.println(opt.isEmpty());
		System.err.println(opt.isPresent());
		System.err.println(opt.orElse("banana"));
	}
	
	
	
	@Test
	void of와ofNullable의차이() {
		String str = null;
//		Optional<String> opt1 = Optional.of(str);
		Optional<String> opt2 = Optional.ofNullable(str);
	}
	@Test
	void 빈객체를사용하는경우() {
		Optional<String> opt = Optional.ofNullable(null);
//		System.out.println(opt.get()); // ERROR
	}
	
	
	
	@Test
	void if를사용하여null값체크하기() {
	String str = "banana";
	if(str != null) {
		System.out.println("값이 존재합니다");
	}
	}
	@Test
	void optional을사용하여null값체크하기() {
		String str = "banana";
		Optional<String> opt = Optional.ofNullable(str);
		opt.ifPresent(name -> System.out.println("값이 존재합니다"));
	}


}
