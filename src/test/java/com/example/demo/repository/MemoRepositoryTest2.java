package com.example.demo.repository;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.Memo;

@SpringBootTest
public class MemoRepositoryTest2 {

	@Autowired
	MemoRepository2 memoRepasitory;

	@Test
	public void 번호가3보다작은_메모검색() {
		// 테이블 삭제하고 메모 3건 다시 추가
		List<Memo> list = memoRepasitory.findByNoLessThan(3);
		for (Memo memo : list) {
			System.out.println(memo);
		}
	}

	@Test
	public void 텍스트가null이아닌_메모검색() {
		List<Memo> list = memoRepasitory.findByTextIsNotNull();
		for (Memo memo : list) {
			System.out.println(memo);
		}
	}

	@Test
	public void 번호가2와3사이인_메모검색() {
		List<Memo> list = memoRepasitory.findByNoBetween(2, 3);
		for (Memo memo : list) {
			System.out.println(memo);
		}
	}

	@Test
	public void 번호를기준으로역정렬한_메모검색() {
		List<Memo> list = memoRepasitory.findAllByOrderByNoDesc();
		for (Memo memo : list) {
			System.out.println(memo);
		}
	}

	@Test

	public void 번호가3보다작은_메모삭제() {
		memoRepasitory.deleteMemoByNoLessThan(3);
	}
}
