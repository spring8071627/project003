package com.example.demo.repository;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.Book;

@SpringBootTest
public class BookRepositoryTest2 {

	@Autowired
	BookRepository2 bookRepository2;
	
	@Test
	public void 자바프로그래밍책이아닌거을검색() {
		List<Book> list = bookRepository2.get1("자바프로그래밍입문");
		for(Book book : list) {
			System.out.println(book);
		}
	}
}
