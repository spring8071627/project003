package com.example.demo.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.Order;

@SpringBootTest
public class OrderRepositoryTest {
	@Autowired
	OrderRepository orderRepository;
	
	@Test
	public void 데이터등록() {
		Order order1 = new Order(0,"둘리", null, "인천 구월동");
		orderRepository.save(order1);
		
		Order order2 = new Order(0,"또치", null, "인천 동래동");
		orderRepository.save(order2);
	}

}
