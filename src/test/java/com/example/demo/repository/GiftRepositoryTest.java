package com.example.demo.repository;



import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.Gift;

@SpringBootTest
public class GiftRepositoryTest {

	@Autowired
	GiftRepository giftRepsitory;
	
	@Test
	public void 가격이50000원이상인선물() {
		List<Gift> list = giftRepsitory.get1(50000);
		for(Gift gift : list) {
			System.out.println(gift);
		}
	}
}
